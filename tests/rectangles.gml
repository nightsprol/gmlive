#define main

// Constants
NUM = 128;

// Set up all the stuff
rectangles = rand_rects(NUM, 512, 512);

#define step
for(var i = 0; i < NUM; i++) {
    if(mouse_check_button(mb_left)) {
        rectangles[i].dx = lerp(rectangles[i].dx, mouse_x, 0.1);
        rectangles[i].dy = lerp(rectangles[i].dy, mouse_y, 0.1);
    } else {
        rectangles[i].dx += random_range(-12, 12);
        rectangles[i].dy += random_range(-12, 12);
    }
}

#define draw

for(var i = 0; i < NUM; i++) {
    draw_rect(rectangles[i]);
}

#define rand_rect(WIDTH, HEIGHT)
var rectangle = { dx:0, dy:0, w:0, h:0, c:0, a:0 };

rectangle.w     = random_range(30, 110);
rectangle.h     = random_range(15, 90);
rectangle.dx    = random_range(0, WIDTH - rectangle.w);
rectangle.dy    = random_range(0, HEIGHT - rectangle.h);
rectangle.c     = make_color_rgb(random(255), random(255), random(255));
rectangle.a     = random_range(0.1, 0.4);

return rectangle;

#define rand_rects(NUM, WIDTH, HEIGHT)
var rects = [];

for(var i = 0; i < NUM; i++) {
    rects[i] = rand_rect(WIDTH, HEIGHT)
}

return rects;

#define draw_rect(rectangle)
draw_set_color(rectangle.c);
draw_set_alpha(rectangle.a);
draw_rectangle(rectangle.dx - (rectangle.w/2), rectangle.dy - (rectangle.h/2),
    rectangle.dx + (rectangle.w/2),
    rectangle.dy + (rectangle.h/2),
    false
);
draw_