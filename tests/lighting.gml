#define main

// We need a light and dark surface
// We also need various light sources
// One at the mouse and the others placed randomly

// Set up the room
room_width  = 720;
room_height = 720;
room_speed  = 60;

// Create the surfaces
shadows = surface_create(room_width, room_height);

// Create the light sources
light   = create_light(120, 0.6, 8, 16);

torches = [];
NUM_TORCHES = 10;

for(var i = 0; i < NUM_TORCHES; i++) {
    torches[i] = create_light(
        irandom_range(64, 128),
        random_range(0.2, 0.6),
        3,
        irandom_range(7, 12)
    );
    torches[i].dx = random_range(0, room_width);
    torches[i].dy = random_range(0, room_height);
}

#define step
light.dx = mouse_x;
light.dy = mouse_y;

#define draw

surface_set_target(shadows);
draw_clear(c_black);

draw_light(light);
for(var i = 0; i < NUM_TORCHES; i++)
    draw_light(torches[i]);

surface_reset_target();

draw_surface(shadows, 0, 0);

#define create_light(radius, alpha, layers, points)
return { radius: radius, dx: 0, dy: 0, alpha:alpha, layers:layers, points:points };

#define draw_light(light)
draw_set_blend_mode(bm_subtract);
draw_set_color(c_white);

for(var i = light.layers; i > 0; i--) {
    var radius = light.radius + (i / light.layers) * irandom(4);

    draw_set_alpha(light.alpha);
    draw_primitive_begin(pr_trianglefan);

    for(var j = 1; j <= light.points; j++) {
        var rad = degtorad((i / light.layers) * 360 + (j / light.points) * 360);
        var dx  = light.dx + radius * cos(rad);
        var dy  = light.dy + radius * sin(rad);

        draw_vertex(dx, dy);
    }

    draw_primitive_end();
}

draw_set_blend_mode(bm_normal);
draw_set_alpha(1);