#define main

DELTA_TIME  = 750;

MAX_SPEED   = 1;

WIDTH       = 1280;
HEIGHT      = 720;

PEN_WIDTH   = 5;
POINTS      = 48;

AMPLITUDE   = WIDTH / 4;

TOPX = WIDTH / 2;
TOPY = 100;
BOTX = WIDTH / 2;
BOTY = HEIGHT - 100;

#define draw

var spd = MAX_SPEED * cos(current_time / DELTA_TIME);

draw_text(10, 10, "Amplitude: " + string(AMPLITUDE * cos(2 * pi * spd)));
draw_text(10, 40, "FPS: " + string(fps));

var bx   = TOPX + AMPLITUDE * cos(2 * pi * spd);
var by   = HEIGHT / 2;

draw_bezier_curve(TOPX, TOPY, BOTX, BOTY, bx, by, POINTS, PEN_WIDTH);

#define draw_bezier_curve
///draw_bezier_curve(startX, startY, endX, endY, bezierX, bezierY, [points], [width])

var startX  = argument[0];
var startY  = argument[1];
 
var endX    = argument[2];
var endY    = argument[3];
 
var bezierX = argument[4];
var bezierY = argument[5];

// Defaults
var points  = 100;
var width   = 1;

// Cascade arguments
switch(argument_count) {
    case 8: width   = argument[7];
    case 7: points  = argument[6];
}

var x1, y1;
var x2 = startX, y2 = startY;

for(var i = 1; i <= points; i++) {
    var f = i / points;     // fraction
    var fin = 1 - f;        // fraction inverse
    
    x1 = x2;                // xprevious
    y1 = y2;                // yprevious
    
    x2 = floor( (fin * fin * startX)
        + (2 * fin * f * bezierX) + (f * f * endX) );
    y2 = floor( (fin * fin * startY)
        + (2 * fin * f * bezierY) + (f * f * endY) );
    
    draw_line_width(x1, y1, x2, y2, width);
}