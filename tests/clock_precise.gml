#define main

// CONSTANTS
WIDTH   = 720;
HEIGHT  = 720;
xpos    = 32;
ypos    = 32;

second_init = current_time;
prev_sec    = current_second;

RADIUS      = 256;

// Finally make the clock
clock   = make_clock(RADIUS);

#define draw

if(prev_sec != current_second) {
    second_init = current_time;
    prev_sec = current_second;
}

// 60 seconds per minute
// This means we want to scale 6 degrees by [0, 1000)

var seconds_delta   = (current_time - second_init) * 0.006;
var minutes_delta   = (current_second * 0.1);
var hours_delta     = (current_minute * 0.5);

var seconds_rad = degtorad((current_second * 6) - 90 + seconds_delta);
var sx      = xpos + RADIUS + SECOND_HAND_LENGTH * cos(seconds_rad);
var sy      = ypos + RADIUS + SECOND_HAND_LENGTH * sin(seconds_rad);

var minutes_rad = degtorad((current_minute * 6) - 90 + minutes_delta);
var mx      = xpos + RADIUS + MINUTE_HAND_LENGTH * cos(minutes_rad);
var my      = ypos + RADIUS + MINUTE_HAND_LENGTH * sin(minutes_rad);

var hours_rad = degtorad((current_hour * 30) - 90 + hours_delta);
var hx      = xpos + RADIUS + HOUR_HAND_LENGTH * cos(hours_rad);
var hy      = ypos + RADIUS + HOUR_HAND_LENGTH * sin(hours_rad);

// Draw the hands of the clock
line(xpos + RADIUS, ypos + RADIUS, mx, my, MINUTE_HAND_WIDTH, c_black);
line(xpos + RADIUS, ypos + RADIUS, hx, hy, HOUR_HAND_WIDTH, c_black);
line(xpos + RADIUS, ypos + RADIUS, sx, sy, SECOND_HAND_WIDTH, c_black);

// Debugging
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_color(c_black);
draw_text(10, 10, ""
    + "Current Second: " + string(current_second)
    + "#Seconds Delta: " + string(seconds_delta)
    + "#Minutes Delta: " + string(minutes_delta)
    + "#Hours Delta: " + string(hours_delta)
);

/*              FUNCTIONS               */

#define circle(x, y, radius, outline, color)
draw_set_color(color);
draw_circle(x, y, radius, outline);

#define line(x1, y1, x2, y2, width, color)
draw_set_color(color);
draw_line_width(x1, y1, x2, y2, width);

#define distance(x1, y1, x2, y2)
return sqrt(power(x2 - x1, 2) + power(y2 - y1, 2));

#define in_circle(x, y, radius, dx, dy)
return distance(x, y, dx, dy) < radius;

#define make_clock(radius)

/*      The following are all a function of the arguments above     */

SECOND_HAND_LENGTH  = radius / 1.3;
SECOND_HAND_WIDTH   = max(1, radius / 128);

MINUTE_HAND_LENGTH  = radius / 1.46;
MINUTE_HAND_WIDTH   = max(2, radius / 64);

HOUR_HAND_LENGTH    = radius / 2.32;
HOUR_HAND_WIDTH     = max(3, radius / 51);