#define main

XORIGIN     = 256;
YORIGIN     = 256;

RADIUS      = 64;

XOFFSET     = 20;
YOFFSET     = 16;

mouth_size  = -32;


#define step
if(keyboard_check_direct(vk_up))
    mouth_size--;
if(keyboard_check_direct(vk_down))
    mouth_size++;

#define draw

draw_circle(XORIGIN, YORIGIN, RADIUS, true);
draw_circle(XORIGIN - XOFFSET, YORIGIN - YOFFSET, 8, true);
draw_circle(XORIGIN + XOFFSET, YORIGIN - YOFFSET, 8, true);

draw_bezier_curve(XORIGIN - XOFFSET - 10, YORIGIN + YOFFSET, XORIGIN + XOFFSET + 10, YORIGIN + YOFFSET, 
    XORIGIN, YORIGIN + YOFFSET + mouth_size, 100);

#define draw_bezier_curve
///draw_bezier_curve(startX, startY, endX, endY, bezierX, bezierY, [points], [width])

var startX  = argument[0];
var startY  = argument[1];
 
var endX    = argument[2];
var endY    = argument[3];
 
var bezierX = argument[4];
var bezierY = argument[5];

// Defaults
var points  = 100;
var width   = 1;

// Cascade arguments
switch(argument_count) {
    case 8: width   = argument[7];
    case 7: points  = argument[6];
}

var px, py;
var dx = startX, dy = startY;

for(var i = 1; i <= points; i++) {
    var f = i / points;     // fraction
    var fin = 1 - f;        // fraction inverse
    
    px = dx;               // xprevious
    py = dy;               // yprevious
    
    dx = floor( (fin * fin * startX)
        + (2 * fin * f * bezierX) + (f * f * endX) );
    dy = floor( (fin * fin * startY)
        + (2 * fin * f * bezierY) + (f * f * endY) );
    
    draw_line_width(px, py, dx, dy, width);
}