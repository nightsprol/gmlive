#define main

/*                      GAME                        */
room_speed  = 60;
room_width  = 720;
room_height = 720;

game_over   = false;

/*                      PLAYER                      */
// Player Constants
WALK_ACCEL  = 1;
FRICTION    = 0.4;
MAX_SPEED   = 12;
// Variables
px          = 100;
py          = 100;
hspd        = 0;
vspd        = 0;
// Drawing and collisions
WIDTH       = 16;
HEIGHT      = 16;
// Key controls
k_left = ord('A');  k_right = ord('D');
k_up = ord('W');    k_down = ord('S');

/*                      OPPONENT                    */
bullets     = ds_list_create();
B_ACCEL     = 1;
B_MAX_SPD   = 8;

#define step

// Walking
if(keyboard_check_direct(k_left))
    hspd = approach(hspd, -MAX_SPEED, WALK_ACCEL);
else if(keyboard_check_direct(k_right))
    hspd = approach(hspd, MAX_SPEED, WALK_ACCEL);
else
    hspd = approach(hspd, 0, FRICTION);

if(keyboard_check_direct(k_up))
    vspd = approach(vspd, -MAX_SPEED, WALK_ACCEL);
else if(keyboard_check_direct(k_down))
    vspd = approach(vspd, MAX_SPEED, WALK_ACCEL);
else
    vspd = approach(vspd, 0, FRICTION);

// Update position
if(!game_over) {
    px += hspd;
    py += vspd;

    // Now for the attacking player!!
    if(mouse_check_button(mb_left)) {
        // x, y, velocity, direction
        var bullet = [mouse_x, mouse_y, 0,
            degtorad(point_direction(mouse_x, mouse_y, px, py))
        ];
        ds_list_add(bullets, bullet);
    }
}

// Update bullets
for(var i = 0; i < ds_list_size(bullets); i++) {
    var bullet = ds_list_find_value(bullets, i);
    ds_list_delete(bullets, i);

    var v = approach(bullet[2], B_MAX_SPD, B_ACCEL);

    var hor = v * cos(bullet[3]);
    var ver = -v * sin(bullet[3]);

    bullet[0] += hor;
    bullet[1] += ver;
    bullet[2] = v;

    // Check collision
    if(bullet[0] > px - WIDTH/2 && bullet[0] < px + WIDTH/2
            && bullet[1] > py - HEIGHT/2 && bullet[1] < py + HEIGHT/2)
        game_over = true;

    if(!game_over && !outside_room(bullet[0], bullet[1]))
        ds_list_add(bullets, bullet);
}

#define draw

draw_rectangle(px - (WIDTH/2), py - (HEIGHT/2), px + (WIDTH/2), py + (HEIGHT/2), false);

for(var i = 0; i < ds_list_size(bullets); i++) {
    var bullet = ds_list_find_value(bullets, i);
    draw_circle(bullet[0], bullet[1], 2, false);
}

if(game_over) {
    draw_set_halign(fa_center);
    draw_set_valign(fa_middle);
    draw_text_transformed(room_width/2, room_height/2, "GAME OVER", 4, 4, 0);
}

draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_text(10, 10, "FPS: " + string(fps));

#define approach(a, b, amt)
if(a < b)
    return min(a + amt, b);
return max(a - amt, b);

#define outside_room(x, y)
return (x < 0 || x > room_width || y < 0 || y > room_height);