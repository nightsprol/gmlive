///draw_text_wavy(x, y, text, height, [sep], [wavelength], [time])
// Arguments in [brackets] are optional

// Variables
var drawX, drawY, text, height, sep, wavelength, time;
drawX       = argument[0];
drawY       = argument[1];
text        = argument[2];
height      = argument[3];  // Amplitude of the wave
sep         = 0;            // Separation between chars
wavelength  = 2 * pi;       // 2*pi is one full wavelength
time        = 500;          // [1 -- X], larger means slower

// Set optional arguments if they are provided
if(argument_count >= 5) { sep = argument[4]; }
if(argument_count >= 6) { wavelength = argument[5]; }
if(argument_count >= 7) { time = argument[6]; }

draw_set_valign(fa_center);

for(var i = 1; i <= string_length(text); i++) {
    var char = string_char_at(text, i);
    var dy = drawY + (height * sin((i * wavelength / string_length(text))
            + (current_time / time)));

    draw_text(drawX, dy, string_char_at(text, i));

    drawX += string_width(char) + sep;
}