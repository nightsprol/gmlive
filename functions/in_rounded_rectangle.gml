/*
    The functions defined in this GMLive script are as follows:

        1) distance(x1, y1, x2, y2)
            Your standard distance formula for two points

        2) in_square(x, y, radius, dx, dy)
            Checks if the point (dx, dy) is in the square of size radius at centered at (x, y)

        3) in_circle(x, y, radius, dx, dy)
            Checks if the point (dx, dy) is in the circle of size radius at (x, y)

        4) in_rounded_rectangle(x, y, radius, round_radius, dx, dy)
            Checks if the point (dx, dy) is in the rounded rectangle of size radius centered at (x, y)
            round_radius defines how much to round the corners of the rectangle

        5) in_rounded_rectangle2(x, y, radius, size, outer_round_radius, inner_round_radius, dx, dy)
            Checks if the point (dx, dy) is in the rounded rectangle region centered around (x, y)
            This is done by doing two calls to in_rounded_rectangle() on the two different rounded rectangles
*/

#define distance(x1, y1, x2, y2)
/// Returns the distance between 2 points
return sqrt(power(x2 - x1, 2) + power(y2 - y1, 2));

#define in_square(x, y, radius, dx, dy)
///Returns true if point (dx, dy) is in the square centered around (x, y)
return (dx > x - radius && dx < x + radius
    && dy > y - radius && dy < y + radius);

#define in_circle(x, y, radius, dx, dy)
///Returns true if point (dx, dy) is in the circle centered around (x, y)
return distance(x, y, dx, dy) < radius;

#define in_rounded_rectangle(x, y, radius, round_radius, dx, dy)
///Returns true if the point (dx, dy) is in the rounded rectangle centered around (x, y)
/*
    We want to check if dx, dy are within the specified rounded rectangle region
    We do this using a square and 4 circles
*/

// Check if the point is outside the rectangle
if(!in_square(x, y, radius, dx, dy))
    return false;

// Check the rounded edges using circles
/*
    This looks complicated, but basically it checks the 4 corners of the square
        and if the point lies past the origin of the circle it must then also
        lie within the radius of the circle.

    h and v are only ever -1 or 1, so we can do a sign comparison instead of
        needing to do multiple unique inequality checks for each corner.
*/
for(var h = -1; h <= 1; h += 2) {
    for(var v = -1; v <= 1; v += 2) {
        var origin_x = x + (h * radius) - (h * round_radius);
        var origin_y = y + (v * radius) - (v * round_radius);

        if(sign(dx - origin_x) == h && sign(dy - origin_y) == v
            && !in_circle(origin_x, origin_y, round_radius, dx, dy))
            return false;
    }
}

// All cases have been considered, it must be inside the rounded rectangle
return true;

#define in_rounded_rectangle2(x, y, radius, size, outer_round_radius, inner_round_radius, dx, dy)
///Returns true if the point (dx, dy) is in the rounded rectangle region centered around (x, y)

// Check if the point is outside the outer rounded rectangle
if(!in_rounded_rectangle(x, y, radius, outer_round_radius, dx, dy))
    return false;

// Check if the point is not inside the inner rounded rectangle
return !in_rounded_rectangle(x, y, radius - size, inner_round_radius, dx, dy);