/*
    GML Easings
    Functions taken from https://easings.net/
*/

// Per-surface constants
BASE_WIDTH      = 160;  // Base width of each easing canvas
BASE_HEIGHT     = 90;   // Base height of each easing canvas
OFFSET          = 40;   // Extra offset to width/height of each easing canvas
STEP_SIZE       = 0.01; // How precisely should we render each canvas? Smaller = better precision; 0.01 means 100 steps to render
CONST_TIME      = 50;   // How fast should the animation run? Smaller = faster
POINTER_RADIUS  = 4;    // How big to make pointer icon (arrow-circle)

// Constants for rendering all surfaces in a grid
GRID_COLS       = 6;    // How many easing graphs to render in a single row
GRID_SEPARATION = 10;   // How many pixels of padding to have between the border and easing graphs

// Colors used to render the easing canvases
COLOR_0_A = c_white;
COLOR_0_B = make_color_rgb(10, 82, 58);
COLOR_1_A = make_color_rgb(40, 64, 94);
COLOR_1_B = c_white;
COLOR_2_A = COLOR_1_A;
COLOR_2_B = COLOR_0_B;

// All easing functions
easings = [
    create_easing_surface("easeInSine", COLOR_0_A, COLOR_0_B),
    create_easing_surface("easeOutSine", COLOR_1_A, COLOR_1_B),
    create_easing_surface("easeInOutSine", COLOR_2_A, COLOR_2_B),
    create_easing_surface("easeInQuad", COLOR_0_A, COLOR_0_B),
    create_easing_surface("easeOutQuad", COLOR_1_A, COLOR_1_B),
    create_easing_surface("easeInOutQuad", COLOR_2_A, COLOR_2_B),
    create_easing_surface("easeInCubic", COLOR_0_A, COLOR_0_B),
    create_easing_surface("easeOutCubic", COLOR_1_A, COLOR_1_B),
    create_easing_surface("easeInOutCubic", COLOR_2_A, COLOR_2_B),
    create_easing_surface("easeInQuart", COLOR_0_A, COLOR_0_B),
    create_easing_surface("easeOutQuart", COLOR_1_A, COLOR_1_B),
    create_easing_surface("easeInOutQuart", COLOR_2_A, COLOR_2_B),
    create_easing_surface("easeInQuint", COLOR_0_A, COLOR_0_B),
    create_easing_surface("easeOutQuint", COLOR_1_A, COLOR_1_B),
    create_easing_surface("easeInOutQuint", COLOR_2_A, COLOR_2_B),
    create_easing_surface("easeInExpo", COLOR_0_A, COLOR_0_B),
    create_easing_surface("easeOutExpo", COLOR_1_A, COLOR_1_B),
    create_easing_surface("easeInOutExpo", COLOR_2_A, COLOR_2_B),
    create_easing_surface("easeInCirc", COLOR_0_A, COLOR_0_B),
    create_easing_surface("easeOutCirc", COLOR_1_A, COLOR_1_B),
    create_easing_surface("easeInOutCirc", COLOR_2_A, COLOR_2_B),
    create_easing_surface("easeInBack", COLOR_0_A, COLOR_0_B),
    create_easing_surface("easeOutBack", COLOR_1_A, COLOR_1_B),
    create_easing_surface("easeInOutBack", COLOR_2_A, COLOR_2_B),
    create_easing_surface("easeInElastic", COLOR_0_A, COLOR_0_B),
    create_easing_surface("easeOutElastic", COLOR_1_A, COLOR_1_B),
    create_easing_surface("easeInOutElastic", COLOR_2_A, COLOR_2_B),
    create_easing_surface("easeInBounce", COLOR_0_A, COLOR_0_B),
    create_easing_surface("easeOutBounce", COLOR_1_A, COLOR_1_B),
    create_easing_surface("easeInOutBounce", COLOR_2_A, COLOR_2_B),

    create_easing_surface("linear", COLOR_0_A, COLOR_0_B),
];

/******************************************
            EASING FUNCTIONS
******************************************/

#define easeInSine(x)
return 1 - cos((x * pi) / 2);

#define easeOutSine(x)
return sin((x * pi) / 2);

#define easeInOutSine(x)
return -(cos(pi * x) - 1) / 2;

#define easeInQuad(x)
return x * x;

#define easeOutQuad(x)
return 1 - (1 - x) * (1 - x);

#define easeInOutQuad(x)
return x < 0.5 ? 2 * x * x : 1 - power(-2 * x + 2, 2) / 2;

#define easeInCubic(x)
return x * x * x;

#define easeOutCubic(x)
return 1 - power(1 - x, 3);

#define easeInOutCubic(x)
return x < 0.5 ? 4 * x * x * x : 1 - power(-2 * x + 2, 3) / 2;

#define easeInQuart(x)
return x * x * x * x;

#define easeOutQuart(x)
return 1 - power(1 - x, 4);

#define easeInOutQuart(x)
return x < 0.5 ? 8 * x * x * x * x : 1 - power(-2 * x + 2, 4) / 2;

#define easeInQuint(x)
return x * x * x * x * x;

#define easeOutQuint(x)
return 1 - power(1 - x, 5);

#define easeInOutQuint(x)
return x < 0.5 ? 16 * x * x * x * x * x : 1 - power(-2 * x + 2, 5) / 2;

#define easeInExpo(x)
return x == 0 ? 0 : power(2, 10 * x - 10);

#define easeOutExpo(x)
return x == 1 ? 1 : 1 - power(2, -10 * x);

#define easeInOutExpo(x)
return x == 0
  ? 0
  : x == 1
  ? 1
  : x < 0.5 ? power(2, 20 * x - 10) / 2
  : (2 - power(2, -20 * x + 10)) / 2;

#define easeInCirc(x)
return 1 - sqrt(1 - power(x, 2));

#define easeOutCirc(x)
return sqrt(1 - power(x - 1, 2));

#define easeInOutCirc(x)
return x < 0.5
  ? (1 - sqrt(1 - power(2 * x, 2))) / 2
  : (sqrt(1 - power(-2 * x + 2, 2)) + 1) / 2;

#define easeInBack(x)
var c1 = 1.70158;
var c3 = c1 + 1;

return c3 * x * x * x - c1 * x * x;

#define easeOutBack(x)
var c1 = 1.70158;
var c3 = c1 + 1;

return 1 + c3 * power(x - 1, 3) + c1 * power(x - 1, 2);

#define easeInOutBack(x)
var c1 = 1.70158;
var c2 = c1 * 1.525;

return x < 0.5
  ? (power(2 * x, 2) * ((c2 + 1) * 2 * x - c2)) / 2
  : (power(2 * x - 2, 2) * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2;

#define easeInElastic(x)
var c4 = (2 * pi) / 3;

return x == 0
  ? 0
  : x == 1
  ? 1
  : -power(2, 10 * x - 10) * sin((x * 10 - 10.75) * c4);

#define easeOutElastic(x)
var c4 = (2 * pi) / 3;

return x == 0
  ? 0
  : x == 1
  ? 1
  : power(2, -10 * x) * sin((x * 10 - 0.75) * c4) + 1;

#define easeInOutElastic(x)
var c5 = (2 * pi) / 4.5;

return x == 0
  ? 0
  : x == 1
  ? 1
  : x < 0.5
  ? -(power(2, 20 * x - 10) * sin((20 * x - 11.125) * c5)) / 2
  : (power(2, -20 * x + 10) * sin((20 * x - 11.125) * c5)) / 2 + 1;

#define easeInBounce(x)
return 1 - easeOutBounce(1 - x);

#define easeOutBounce(x)
var n1 = 7.5625;
var d1 = 2.75;

if (x < 1 / d1) {
    return n1 * x * x;
} else if (x < 2 / d1) {
    x -= 1.5 / d1;
    return n1 * x * x + 0.75;
} else if (x < 2.5 / d1) {
    x -= 2.25 / d1;
    return n1 * x * x + 0.9375;
} else {
    x -= 2.625 / d1;
    return n1 * x * x + 0.984375;
}

#define easeInOutBounce(x)
return x < 0.5
  ? (1 - easeOutBounce(1 - 2 * x)) / 2
  : (1 + easeOutBounce(2 * x - 1)) / 2;

#define linear(x)
return x;

/******************************************
                  OTHER
******************************************/

#define call_easing_function(func_name, x)
if (func_name == "easeInSine") {
    return easeInSine(x);
} else if (func_name == "easeOutSine") {
    return easeOutSine(x);
} else if (func_name == "easeInOutSine") {
    return easeInOutSine(x);
} else if (func_name == "easeInCubic") {
    return easeInCubic(x);
} else if (func_name == "easeInQuad") {
    return easeInQuad(x);
} else if (func_name == "easeOutQuad") {
    return easeOutQuad(x);
} else if (func_name == "easeInOutQuad") {
    return easeInOutQuad(x);
} else if (func_name == "easeInCubic") {
    return easeInCubic(x);
} else if (func_name == "easeOutCubic") {
    return easeOutCubic(x);
} else if (func_name == "easeInOutCubic") {
    return easeInOutCubic(x);
} else if (func_name == "easeInQuart") {
    return easeInQuart(x);
} else if (func_name == "easeOutQuart") {
    return easeOutQuart(x);
} else if (func_name == "easeInOutQuart") {
    return easeInOutQuart(x);
} else if (func_name == "easeInQuint") {
    return easeInQuint(x);
} else if (func_name == "easeOutQuint") {
    return easeOutQuint(x);
} else if (func_name == "easeInOutQuint") {
    return easeInOutQuint(x);
} else if (func_name == "easeInExpo") {
    return easeInExpo(x);
} else if (func_name == "easeOutExpo") {
    return easeOutExpo(x);
} else if (func_name == "easeInOutExpo") {
    return easeInOutExpo(x);
} else if (func_name == "easeInCirc") {
    return easeInCirc(x);
} else if (func_name == "easeOutCirc") {
    return easeOutCirc(x);
} else if (func_name == "easeInOutCirc") {
    return easeInOutCirc(x);
} else if (func_name == "easeInBack") {
    return easeInBack(x);
} else if (func_name == "easeOutBack") {
    return easeOutBack(x);
} else if (func_name == "easeInOutBack") {
    return easeInOutBack(x);
} else if (func_name == "easeInElastic") {
    return easeInElastic(x);
} else if (func_name == "easeOutElastic") {
    return easeOutElastic(x);
} else if (func_name == "easeInOutElastic") {
    return easeInOutElastic(x);
} else if (func_name == "easeInBounce") {
    return easeInBounce(x);
} else if (func_name == "easeOutBounce") {
    return easeOutBounce(x);
} else if (func_name == "easeInOutBounce") {
    return easeInOutBounce(x);
} else if (func_name == "linear") {
    return linear(x);
}
show_error("Unrecognized easing function: " + func_name, false);

#define draw
for (var i = 0; i < array_length_1d(easings); ++i) {
    // Calculate offset for the top-left corner of the easing surface
    var base_x, base_y;
    base_x = GRID_SEPARATION + (GRID_SEPARATION + easings[i].width + easings[i].offset * 2) * (i % GRID_COLS);
    base_y = GRID_SEPARATION + (GRID_SEPARATION + easings[i].height + easings[i].offset * 2) * (floor(i / GRID_COLS));

    // Draw the easing surface
    draw_surface(easings[i].surf, base_x, base_y);

    // Draw the pointer at the current y position for the graph
    var dt, dy;
    dt = ((current_time / CONST_TIME) % CONST_TIME) / CONST_TIME;
    dy = easings[i].height - (easings[i].height * call_easing_function(easings[i].name, dt));

    var pointer_x = base_x + easings[i].offset + easings[i].width + POINTER_RADIUS * 2;
    var pointer_y = base_y + dy + easings[i].offset;
    var color = merge_color(easings[i].color_a, easings[i].color_b, dt);

    draw_circle_color(pointer_x, pointer_y, POINTER_RADIUS, color, color, false);
    draw_triangle_color(
        pointer_x - POINTER_RADIUS * 2,
        pointer_y,
        pointer_x,
        pointer_y - POINTER_RADIUS,
        pointer_x,
        pointer_y + POINTER_RADIUS,
        color,
        color,
        color,
        false,
    );
}

#define create_easing_surface(func_name, color_a, color_b)
var surf = surface_create(BASE_WIDTH + OFFSET * 2, BASE_HEIGHT + OFFSET * 2);
surface_set_target(surf);
draw_rectangle_color(0, 0, surface_get_width(surf), surface_get_height(surf), c_black, c_black, c_black, c_black, false);
draw_set_alpha(0.8);
draw_line_width_color(OFFSET, OFFSET, OFFSET, BASE_HEIGHT + OFFSET, 2, c_gray, c_gray);
draw_line_width_color(OFFSET, BASE_HEIGHT + OFFSET, BASE_WIDTH + OFFSET, BASE_HEIGHT + OFFSET, 2, c_gray, c_gray);
draw_set_alpha(1.0);

var px = noone, py = noone;
for (var i = 0.0; i <= 1.0; i += STEP_SIZE) {
    var dt, dx, dy;
    dx = BASE_WIDTH * i;
    dy = BASE_HEIGHT - (BASE_HEIGHT * call_easing_function(func_name, i));
    var color = merge_color(color_a, color_b, i);

    if (dx > px && px != noone && py != noone) {
        draw_line_width_color(px + OFFSET, py + OFFSET, dx + OFFSET, dy + OFFSET, 3, color, color);
    }
    px = dx;
    py = dy;
}

draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_text_color((BASE_WIDTH + OFFSET * 2) / 2, BASE_HEIGHT + OFFSET * 1.5, func_name, c_white, c_white, c_white, c_white, 1.0);

surface_reset_target();

var easing = {
    surf: surf,
    width: BASE_WIDTH,
    height: BASE_HEIGHT,
    offset: OFFSET,
    step_size: STEP_SIZE,
    color_a: color_a,
    color_b: color_b,
    name: func_name
};

return easing;
