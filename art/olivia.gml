#define main

height  = 120;
width   = 80;

canvas = surface_create(1280, 1280);
room_speed = 60;

#define step

#define draw

surface_set_target(canvas);
draw_set_alpha(0.2);
var time = current_time / 800;
// O
var dx1 = 60 + 40 * cos(time);
var dy1 = 60 + 50 * sin(time);

var dx2 = 60 + 40 * sin(time);
var dy2 = 60 + 50 * cos(time);

draw_line_color(dx1, dy1, dx2, dy2, c_blue, c_teal);
// l
dx1 = 110;
dy1 = 60 + 50 * sin(time);
dx2 = 120;
dy2 = 60 + 50 * cos(time / 1.2);
draw_line_color(dx1, dy1, dx2, dy2, c_teal, c_blue);
// bottom of i
dx1 = 140;
dy1 = 80 + 30 * sin(time * 1.2);
dx2 = 155;
dy2 = 80 + 30 * cos(time);
draw_line_color(dx1, dy1, dx2, dy2, c_teal, c_blue);
// dot of i
dx1 = 148 + 7 * cos(time);
dy1 = 35 + 7 * sin(time);
dx2 = 148 + 7 * sin(time);
dy2 = 35 + 7 * cos(time);
draw_line_color(dx1, dy1, dx2, dy2, c_teal, c_blue);
// v
dx1 = 200 + 30 * sin(time);
dy1 = 110 - 45 * abs(sin(time));
dx2 = 200 + 30 * cos(time);
dy2 = 110 - 45 * abs(cos(time));
draw_line_color(dx1, dy1, dx2, dy2, c_teal, c_blue);
// bottom of i
dx1 = 240;
dy1 = 80 + 30 * sin(time * 1.2);
dx2 = 255;
dy2 = 80 + 30 * cos(time);
draw_line_color(dx1, dy1, dx2, dy2, c_teal, c_blue);
// dot of i
dx1 = 248 + 7 * cos(time);
dy1 = 35 + 7 * sin(time);
dx2 = 248 + 7 * sin(time);
dy2 = 35 + 7 * cos(time);
draw_line_color(dx1, dy1, dx2, dy2, c_teal, c_blue);

// tail of the a
dx1 = 315;
dy1 = 90 + 20 * sin(time * 1.2);
dx2 = 325;
dy2 = 90 + 20 * cos(time);
draw_line_color(dx1, dy1, dx2, dy2, c_teal, c_blue);

// now for the circle part of an a
dx1 = 295 + 30 * cos(time);
dy1 = 80 + 30 * sin(time);
dx2 = 295 + 30 * sin(time);
dy2 = 80 + 30 * cos(time);
draw_line_color(dx1, dy1, dx2, dy2, c_teal, c_blue);

surface_reset_target();
draw_set_alpha(1);
draw_surface(canvas, 0, 0);