#define main

/*
    @author Nathan Wood (twitter: @night_wood)
    @date   09/28/17

    NOTE: This is written for GMLive by YellowAfterlife
        http://yal.cc/r/gml/

        You can simply copy + paste into the editor and run it

	This draws the Instagram logo using randomly placed dots.
*/

// Debug variables
DEBUG       = false;

// Speed of drawing
DOTS_PER_FRAME  = 360;

// Canvas size and background color
WIDTH       = 720;
HEIGHT      = 720;
BACK_COLOR  = c_white;

// Drawing constants
SIZE        = 512;      // Size of the logo, SIZE = WIDTH = HEIGHT
DOT_SIZE    = 3;        // Size of the dots (radius)
GRAINY      = false;    // Whether to draw over negative space or not (true = adds noise)
GRAIN_PAD   = 16;       // The padding along the edges of the logo for graininess
ALPHA_RANGE = 0;        // [0, 1]: 0 = no transparency, 1 = fully random transparency

// Color constants
//  probably don't want to change these
SATURATION  = 255;      // Colors are made from HSV (hue, saturation, value)
VALUE       = 255;      //      and the hue is based on the distance from origin

/*
    - START SCALED CONSTANTS -

    Modifying the constants below will mess up the Instagram logo
    They are all a function of the constants defined previously
    Change those instead if you want
*/
RADIUS                  = SIZE / 2;

CENTER_CIRCLE_RADIUS    = SIZE / 8;
CENTER_RING_RADIUS      = SIZE / 5;

FLASH_RADIUS            = (CENTER_RING_RADIUS - CENTER_CIRCLE_RADIUS) / 1.5;
FLASH_DIST              = CENTER_RING_RADIUS;

RECTANGLE_RADIUS        = SIZE / 2.6;
RECTANGLE_SIZE          = SIZE / 12;
RECTANGLE_IN_RADIUS     = RECTANGLE_SIZE * 1.8;
RECTANGLE_OUT_RADIUS    = RECTANGLE_SIZE * 2.6;

LOGO_ROUND_RADIUS       = RADIUS / 2;

MAIN_COLOR_SCALAR       = (480 / SIZE) * 0.10;

xstart = WIDTH / 2;
ystart = HEIGHT / 2;

/*
    - END SCALED CONSTANTS -
*/

// Probably don't want to edit anything below this either
room_speed  = 60;
frame       = 0;
canvas      = surface_create(WIDTH, HEIGHT);

#define draw

// Begin drawing to canvas
surface_set_target(canvas);

for(var i = 0; i < DOTS_PER_FRAME; i++) {

    // Choose a random point within the image, adding some padding if GRAINY is enabled
    var dx = xstart - (RADIUS + (GRAINY ? GRAIN_PAD : 0))
            + random(SIZE + (GRAINY ? GRAIN_PAD * 2 : 0));
    var dy = ystart - (RADIUS + (GRAINY ? GRAIN_PAD : 0))
            + random(SIZE + (GRAINY ? GRAIN_PAD * 2 : 0));

    // Get the distance of the point from the origin and the flash origin
    var dist        = distance(xstart, ystart, dx, dy);
    var dist_flash  = distance(xstart + FLASH_DIST, ystart - FLASH_DIST, dx, dy);

    // Create the colors based on the position of the point randomly chosen
    var o_dist  = distance(xstart - (RADIUS * 2/3), ystart - (RADIUS * 2.3), dx, dy);
    var o_hue   = modulo(modulo(o_dist * MAIN_COLOR_SCALAR, 180) - 212, 360);
    var o_color = make_color_hsv(o_hue, SATURATION, VALUE);

    var y_dist  = distance(xstart - (RADIUS * 2/5), ystart + RADIUS, dx, dy);
    var y_color = make_color_hsv(35, SATURATION, VALUE);

    var color   = o_color;

    // Merge the base color with the yellow color if close to the yellow origin
    if(y_dist < o_dist)
        color = merge_color(color, y_color, (o_dist - y_dist) / (o_dist + y_dist));

    // The following checks define the negative (white) space in the logo
    // If in center white circle
    if(dist > CENTER_CIRCLE_RADIUS && dist < CENTER_RING_RADIUS)
        color = BACK_COLOR;
    // If in flash circle
    else if(dist_flash < FLASH_RADIUS)
        color = BACK_COLOR;
    // If inside rounded rectangle region
    else if(in_rounded_rectangle2(xstart, ystart,
            RECTANGLE_RADIUS, RECTANGLE_SIZE,
            RECTANGLE_OUT_RADIUS, RECTANGLE_IN_RADIUS, dx, dy))
        color = BACK_COLOR;
    // If inside outer rounded rectangle
    else if(!in_rounded_rectangle(xstart, ystart, RADIUS, LOGO_ROUND_RADIUS, dx, dy))
        color = BACK_COLOR;

    // Skip drawing in negative space if GRAINY is not enabled
    if(color == BACK_COLOR && !GRAINY)
        continue;

    draw_set_color(color);
    draw_set_alpha(random(RAND_ALPHA) + (1 - RAND_ALPHA));

    // Draw smaller points for whitespace
    var point_size = color == BACK_COLOR ? floor(DOT_SIZE / 2) : DOT_SIZE;

    draw_circle(dx, dy, point_size <= 1 ? 1 : point_size, false);

}

// Reset surface target
surface_reset_target();

// Draw background color
draw_set_alpha(1);
draw_set_color(BACK_COLOR);
draw_rectangle(0, 0, WIDTH, HEIGHT, false);

// Draw canvas to window
draw_surface(canvas, 0, 0);

// Draw debug stuff
if(DEBUG) {
    draw_set_color(c_black);
    draw_text(10, 0, ""
        + "#FPS: " + string(fps)
        + "#Dots/Frame: " + string(DOTS_PER_FRAME)
        + "#Dots/Sec: " + string(DOTS_PER_FRAME * fps)
        + "#Frames: " + string(frame)
    );
}

frame++;

/*
    - START FUNCTIONS -
*/

#define distance(x1, y1, x2, y2)
/// Returns the distance between 2 points
return sqrt(power(x2 - x1, 2) + power(y2 - y1, 2));

#define modulo(a, b)
///Performs "a modulo b" and supports negative numbers
var r = a % b;
return r < 0 ? r + b : r;

#define in_square(x, y, radius, dx, dy)
///Returns true if point (dx, dy) is in the square centered around (x, y)
return (dx > x - radius && dx < x + radius
    && dy > y - radius && dy < y + radius);

#define in_circle(x, y, radius, dx, dy)
///Returns true if point (dx, dy) is in the circle centered around (x, y)
return distance(x, y, dx, dy) < radius;

#define in_rounded_rectangle(x, y, radius, round_radius, dx, dy)
///Returns true if the point (dx, dy) is in the rounded rectangle centered around (x, y)
/*
    We want to check if dx, dy are within the specified rounded rectangle region
    We do this using a square and 4 circles
*/

// Check if the point is outside the rectangle
if(!in_square(x, y, radius, dx, dy))
    return false;

// Check the rounded edges using circles
/*
    This looks complicated, but basically it checks the 4 corners of the square
        and if the point lies past the origin of the circle it must then also
        lie within the radius of the circle.

    h and v are only ever -1 or 1, so we can do a sign comparison instead of
        needing to do multiple unique inequality checks for each corner.
*/
for(var h = -1; h <= 1; h += 2) {
    for(var v = -1; v <= 1; v += 2) {
        var origin_x = x + (h * radius) - (h * round_radius);
        var origin_y = y + (v * radius) - (v * round_radius);

        if(sign(dx - origin_x) == h && sign(dy - origin_y) == v
            && !in_circle(origin_x, origin_y, round_radius, dx, dy))
            return false;
    }
}

// All cases have been considered, it must be inside the rounded rectangle
return true;

#define in_rounded_rectangle2(x, y, radius, size, outer_round_radius, inner_round_radius, dx, dy)
///Returns true if the point (dx, dy) is in the rounded rectangle region centered around (x, y)

// Check if the point is outside the outer rounded rectangle
if(!in_rounded_rectangle(x, y, radius, outer_round_radius, dx, dy))
    return false;

// Check if the point is not inside the inner rounded rectangle
return !in_rounded_rectangle(x, y, radius - size, inner_round_radius, dx, dy);