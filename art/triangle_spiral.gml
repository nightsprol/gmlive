#define main

height  = 120;
width   = 80;

canvas = surface_create(1280, 1280);
room_speed = 60;

#define draw

var time = current_time / 10000;

draw_clear(c_black);

draw_primitive_begin(pr_trianglestrip);

for(var i = 1; i <= 45; i++) {
    var t = time * i;
    draw_vertex_color(400 + (i * 8) * cos(t), 400 + (i * 8) * sin(t), c_teal, 0.25);
}

draw_primitive_end();