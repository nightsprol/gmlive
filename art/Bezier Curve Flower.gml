#define main

// CONSTANTS
DELTA_TIME  = 800;    // 1 min = 12000 ms, 1 sec = 1000 ms

// CANVAS
WIDTH       = 1280;
HEIGHT      = 720;
BACK_COLOR  = c_white;

surface = surface_create(WIDTH, HEIGHT);

// DRAWING
WAVE_SIZE   = 32;
LEAVES      = 12;

MOVE_RADIUS = 128;

NODE_RADIUS = 10;
PEN_WIDTH   = 8;

xstart      = WIDTH / 2;
ystart      = HEIGHT / 2;

dx          = xstart;
dy          = ystart;
px          = xstart;
py          = ystart;

#define draw

draw_set_color(BACK_COLOR);
draw_rectangle(0, 0, WIDTH, HEIGHT, false);
    
// Set color
var time = current_time / DELTA_TIME;
var color = (time * 5) % 255;
draw_set_color(make_color_hsv(color, color, color));

// Oscillate around circle
var radius_wave = WAVE_SIZE * sin(time * LEAVES);

// UPDATE POSITION
var bx  = px - dx;
var by  = py - dy;
px      = dx;
py      = dy;
dx      = xstart + (MOVE_RADIUS + radius_wave) * cos(time);
dy      = ystart + (MOVE_RADIUS + radius_wave) * sin(time);

// Draw circle
draw_set_alpha(1);
draw_circle(dx, dy, 4 + NODE_RADIUS * abs(sin(time)), false);

// Draw line to surface
surface_set_target(surface);
draw_set_alpha(1);
draw_bezier_curve(px, py, dx, dy, bx, by, 2 + PEN_WIDTH * abs(sin(time)));

// Fade out whole surface slowly
draw_set_alpha(0.01);
draw_set_blend_mode(bm_subtract);
draw_set_color(BACK_COLOR);
draw_rectangle(0, 0, WIDTH, HEIGHT, false);
draw_set_blend_mode(bm_normal);

surface_reset_target();

draw_set_alpha(1);
draw_surface(surface, 0, 0,);

#define draw_bezier_curve
///draw_bezier_curve(startX, startY, endX, endY, bezierX, bezierY, [width])

var startX  = argument[0];
var startY  = argument[1];
 
var endX    = argument[2];
var endY    = argument[3];
 
var bezierX = argument[4];
var bezierY = argument[5];

var width   = (argument_count >= 7 ? argument[6] : 1);

var px, py;
var dx = startX, dy = startY;

for(var t=0.0;t<=1;t+=0.01) {
    px = dx;
    py = dy;
    
    dx = floor(  (1-t)*(1-t)*startX + 2*(1-t)*t*bezierX+t*t*endX);
    dy = floor(  (1-t)*(1-t)*startY + 2*(1-t)*t*bezierY+t*t*endY);
    
    draw_line_width(px, py, dx, dy, width);
}