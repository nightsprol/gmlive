#define main

WIDTH   = 720;
HEIGHT  = 720;

// Constants
DELTA_TIME      = 1000;
DELTA_TIME2     = 1777;

RADIUS          = 128;
WAVE_AMP        = 64;
WAVE_ANG        = 45;
PEN_RADIUS      = 3;
POINTS          = 14;

BACK_COLOR      = c_white;

FADE_FLOAT      = 0.05;

canvas  = surface_create(WIDTH, HEIGHT);

surface_set_target(canvas);
draw_clear(BACK_COLOR);
surface_reset_target();

X_ORIGIN    = (WIDTH / 2);
Y_ORIGIN    = (HEIGHT / 2);

// Room stuff
room_speed      = 120;

#define draw

var time    = current_time / DELTA_TIME;
var time2   = current_time / DELTA_TIME2;

surface_set_target(canvas);

// Fade out previous stuff
if(current_time % 2 == 0) {
    draw_set_alpha(FADE_FLOAT);
    draw_set_color(BACK_COLOR);

    draw_rectangle(0, 0, WIDTH, HEIGHT, false);

    draw_set_alpha(1);
}

for(var i = 0; i < 360; i += (360 / POINTS)) {
    for(var j = 1; j <= 1; j++) {
        var rad = degtorad(i);

        var rad_wave = degtorad(WAVE_ANG * sin(time));

        var rads_radius_wave = degtorad(i * j) + (time * 2);
        var radius_wave = WAVE_AMP * sin(rads_radius_wave);

        var dx  = X_ORIGIN + (RADIUS + radius_wave) * cos(rad + rad_wave);
        var dy  = Y_ORIGIN + (RADIUS + radius_wave) * sin(rad + rad_wave);

        // Now apply the second offset
        var hue     = hash(distance(X_ORIGIN, Y_ORIGIN, dx, dy) * sin(time2), 360);
        var color   = make_color_hsv(hue, 255, 255);

        draw_circle_color(dx, dy, PEN_RADIUS, color, color, false);
    }
}


surface_reset_target();

draw_surface(canvas, 0, 0);

#define distance(x1, y1, x2, y2)
return sqrt(power(x1 - x2, 2) + power(y1 - y2, 2));

#define hash(v, range)
return v % range;