#define main
xstart = 512;
ystart = 512;
angle_speed = 5;

#define step
var radius = 220;
x = xstart + (radius * cos(-current_time / 1000));
y = ystart + (radius * sin(-current_time / 1000));
image_angle += angle_speed;
angle_speed = 5 * sin(current_time / 1000);

#define draw
var nodes = 1 + ((current_time / 1000) % 32);
radius = 4 + (512 * abs(sin(current_time / 1000)));
node_radius = 8;

for(var i = 1; i <= nodes; i++) {
    var color = (i + 1) * 255 / nodes;
    draw_set_color(make_color_hsv(color, color, color));
    
    var a1 = (i * 360 / nodes) + image_angle;
    var a2 = ((i - 1) * 360 / nodes) + image_angle;
    
    var px = x + (radius * cos(degtorad(a1)));
    var py = y + (radius * sin(degtorad(a1)));
    var dx = x + (radius * cos(degtorad(a2)));
    var dy = y + (radius * sin(degtorad(a2)));
    
    
    // Draw line to next node
    if(i < nodes)
        draw_line(px, py, dx, dy);
    else
        draw_line(dx, dy, x + radius, y);
        
    //if(i % 2 == 0) {
        // Draw line away from circle
        
        px = dx + (1280 * cos(degtorad(a1)));
        py = dy + (1280 * sin(degtorad(a1)));
            
        draw_line(dx, dy, px, py);
    //}
    /*else {
        // Draw line into circle
        
        px = dx + (radius * cos(degtorad(180 + a1)));
        py = dy + (radius * sin(degtorad(180 + a1)));
            
        draw_line(dx, dy, x, y);
    }*/
        
    // Draw node
    draw_circle(dx, dy, node_radius, false);
}

var color = current_time % 255;
draw_set_color(make_color_hsv(color, color, color));

// Draw center of circle
//draw_circle(x, y, node_radius, false);