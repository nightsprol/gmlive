#define main

// CONSTANTS
WIDTH   = 1000;
HEIGHT  = 1000;

second_init = current_time;
prev_sec    = current_second;

clocks              = [];
NUM_CLOCKS          = 3;

for(var i = 0; i < NUM_CLOCKS; i++) {
    var hue     = irandom_range(60, 300);
    var color1  = make_color_hsv(hue, 255, 255);
    var color2  = make_color_hsv(hue + 80, 255, 255);
    
    var main_c  = irandom(1) == 0 ? c_white : c_black;
    
    var clock   = make_clock(irandom_range(32, 256), color1, color2, main_c);
    clock.dx    = irandom_range(clock.radius, WIDTH - clock.radius);
    clock.dy    = irandom_range(clock.radius, HEIGHT - clock.radius);
    
    clocks[i]   = clock;
}

#define step
if(mouse_check_button_pressed(mb_left)) {
    for(var i = 0; i < NUM_CLOCKS; i++) {
        var clock = clocks[i];
        
        if(distance(mouse_x, mouse_y, clock.dx, clock.dy) <= clock.radius) {
            clock.moving = true;
            clock.move_xoff = clock.dx - mouse_x;
            clock.move_yoff = clock.dy - mouse_y;
            
            // We want to only get the one on top
            break;
        }
    }
}

if(mouse_check_button(mb_left)) {
    for(var i = 0; i < NUM_CLOCKS; i++) {
        var clock = clocks[i];
        
        if(clock.moving) {
            clock.dx = mouse_x + clock.move_xoff;
            clock.dy = mouse_y + clock.move_yoff;
        }
    }
}

if(mouse_check_button_released(mb_left)) {
    for(var i = 0; i < NUM_CLOCKS; i++) {
        clocks[i].moving = false;
    }
}

#define draw

if(prev_sec != current_second) {
    second_init = current_time;
    prev_sec = current_second;
}

// Draw in reverse order
for(var i = NUM_CLOCKS - 1; i >= 0; i--) {
    draw_clock(clocks[i], second_init);
}

/*              FUNCTIONS               */

#define distance(x1, y1, x2, y2)
return sqrt(power(x2 - x1, 2) + power(y2 - y1, 2));

#define in_circle(x, y, radius, dx, dy)
return distance(x, y, dx, dy) < radius;

#define draw_clock(clock, second_init)
var seconds_delta   = (current_time - second_init) * 0.006;
var minutes_delta   = (current_second * 0.1);
var hours_delta     = (current_minute * 0.5);

var seconds_rad = degtorad((current_second * 6) - 90 + seconds_delta);
var sx      = clock.dx + clock.second_l * cos(seconds_rad);
var sy      = clock.dy + clock.second_l * sin(seconds_rad);

var minutes_rad = degtorad((current_minute * 6) - 90 + minutes_delta);
var mx      = clock.dx + clock.minute_l * cos(minutes_rad);
var my      = clock.dy + clock.minute_l * sin(minutes_rad);

var hours_rad = degtorad((current_hour * 30) - 90 + hours_delta);
var hx      = clock.dx + clock.hour_l * cos(hours_rad);
var hy      = clock.dy + clock.hour_l * sin(hours_rad);

// Draw clock background
draw_surface(clock.clock, clock.dx - clock.radius, clock.dy - clock.radius);

// Draw the hands of the clock
draw_set_color(clock.outline_color);
draw_line_width(clock.dx, clock.dy, hx, hy, clock.hour_w);
draw_line_width(clock.dx, clock.dy, mx, my, clock.minute_w);
draw_line_width(clock.dx, clock.dy, sx, sy, clock.second_w);

#define make_clock(radius, color1, color2, outline_color)

/*      The following are all a function of the arguments above     */

var OUTER_RING_RADIUS   = radius;
var INNER_RING_RADIUS   = radius / 1.05;
var CENTER_RING_RADIUS  = radius / 32;

var SECOND_HAND_LENGTH  = radius / 1.3;
var SECOND_HAND_WIDTH   = max(1, radius / 128);

var MINUTE_HAND_LENGTH  = radius / 1.6;
var MINUTE_HAND_WIDTH   = max(2, radius / 64);

var HOUR_HAND_LENGTH    = radius / 2.32;
var HOUR_HAND_WIDTH     = max(4, radius / 46);

var DOT_OFFSET          = radius / 21;
var NUM_OFFSET          = radius / 8;

var SMALL_DOT_RADIUS    = max(1, radius / 85);
var BIG_DOT_RADIUS      = max(2, radius / 43);

var NUM_SCALE           = radius / 256;

var COLOR_SCALAR        = (256 / radius) * hue_scalar;

var clock = surface_create(radius * 2, radius * 2);
surface_set_target(clock);

draw_circle_color(radius, radius, OUTER_RING_RADIUS, outline_color, outline_color, false);
draw_circle_color(radius, radius, INNER_RING_RADIUS, color1, color2, false);
draw_circle_color(radius, radius, CENTER_RING_RADIUS, outline_color, outline_color, false);

// Draw the numbers and the points to the edge of the circle
draw_set_color(outline_color);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

for(var i = 0; i < 360; i += 6) {
    var dot_x   = radius + (INNER_RING_RADIUS - DOT_OFFSET) * cos(degtorad(i));
    var dot_y   = radius + (INNER_RING_RADIUS - DOT_OFFSET) * sin(degtorad(i));

    var num_x   = radius + (INNER_RING_RADIUS - NUM_OFFSET) * cos(degtorad(i - 60));
    var num_y   = radius + (INNER_RING_RADIUS - NUM_OFFSET) * sin(degtorad(i - 60));

    // Draw the point
    if(i % 5 != 0)
        draw_circle(dot_x, dot_y, SMALL_DOT_RADIUS, true);
    else
        draw_circle(dot_x, dot_y, BIG_DOT_RADIUS, false);

    // Draw the number
    if(i % 5 == 0)
        draw_text_transformed(
            num_x, num_y,
            string((i / 30) + 1),
            NUM_SCALE, NUM_SCALE, 0
        );
}

surface_reset_target();

var clock_obj = {
    clock:      clock,
    dx:         0,
    dy:         0,
    radius:     radius,
    
    outline_color: outline_color,
    second_l:   SECOND_HAND_LENGTH,
    second_w:   SECOND_HAND_WIDTH,
    minute_l:   MINUTE_HAND_LENGTH,
    minute_w:   MINUTE_HAND_WIDTH,
    hour_l:     HOUR_HAND_LENGTH,
    hour_w:     HOUR_HAND_WIDTH,
    
    moving:     false,
    move_xoff:  0,
    move_yoff:  0
};

return clock_obj;