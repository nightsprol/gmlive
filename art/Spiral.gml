#define main
x = 680;
y = 512;

#define step

#define draw
var bkg_color = make_color_hsv((current_time / 75) % 255,
    (current_time / 50) % 255, (current_time / 25) % 255);
draw_set_color(bkg_color);
draw_rectangle(0, 0, 1920, 1080, false);

var circles = 64 + ((current_time / 200 ) % 128);

var MAX_SIZE = 2560;

for(var i = circles; i > 0; i--) {
    // Set color
    var color = i % 200;
    draw_set_color(make_color_hsv(color, color, color));
    
    var RADIUS_WAVE_SIZE = 64;
    
    var MOVE_RADIUS = 128;
    var angle = (i * 16 * pi / circles) 
        + ((current_time / 500) % 360);
    var dx = x + (MOVE_RADIUS * cos(angle));
    var dy = y + (MOVE_RADIUS * sin(angle));
        
    // Draw circle
    var radius = i * MAX_SIZE / circles;
    draw_circle(dx, dy, radius, false);
    
    // Draw inside of circle
    draw_set_color(bkg_color);
    draw_circle(dx, dy, radius * 0.9, false);
}

// Make colors use sin wave, also radius