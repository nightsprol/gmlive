#define main

// CONSTANTS
DELTA_TIME  = 1000;  // ms

// CANVAS
WIDTH       = 1280;
HEIGHT      = 720;

canvas      = surface_create(WIDTH, HEIGHT);

// DRAWING
MOVE_RADIUS = 128;
WAVE_SIZE   = 32;
LEAVES      = 4;

PEN_WIDTH       = 12;
PEN_MIN_WIDTH   = 2;

// MOVEMENT
xstart      = WIDTH / 2;
ystart      = HEIGHT / 2;

px          = xstart;   // previous x
py          = ystart;   // previous y
dx          = xstart;   // current x
dy          = ystart;   // current y

#define step
var time = current_time / DELTA_TIME;

// Oscillate around circle
var radius_wave = WAVE_SIZE * sin(time * LEAVES);

// UPDATE POSITION
px      = dx;
py      = dy;
dx      = xstart + (MOVE_RADIUS + radius_wave) * cos(time);
dy      = ystart + (MOVE_RADIUS + radius_wave) * sin(time);


#define draw
var time = current_time / DELTA_TIME;

// hue = [0, 255], saturation and value = [64, 192]
var hue         = 128 + 128 * sin(time * 0.1);
var saturation  = 128 + 64 * sin(time * 0.1);
var value       = 128 + 64 * sin(time * 0.1);
var color       = make_color_hsv(hue, saturation, value);

draw_set_color(color);

// Begin drawing to canvas
surface_set_target(canvas);

// Draw line to surface
draw_set_alpha(1);
draw_line_width(px, py, dx, dy, PEN_MIN_WIDTH + PEN_WIDTH * abs(sin(time)));

// Fade out whole surface slowly
draw_set_alpha(0.05);
draw_set_blend_mode(bm_subtract);
draw_set_color(c_white);
draw_rectangle(0, 0, WIDTH, HEIGHT, false);
draw_set_blend_mode(bm_normal);

// Reset surface target
surface_reset_target();

// Draw canvas to window
draw_set_alpha(1);
draw_surface(canvas, 0, 0,);

// Draw pen
draw_set_color(color);
draw_set_alpha(1);
draw_circle(dx, dy, (PEN_MIN_WIDTH + PEN_WIDTH * abs(sin(time))) / 2, false);