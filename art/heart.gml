#define main

DT      = 700;

WIDTH   = 640;
HEIGHT  = 480;
canvas  = surface_create(WIDTH, HEIGHT);

// Object stuff
xstart  = WIDTH / 2;
ystart  = HEIGHT / 2;

// Room and debugging stuff
room_speed  = 60;
DEBUG       = false;

#define step

#define draw

surface_set_target(canvas);

var t1 = 1000, t2 = 400, t3 = 300;

var width = 10 + 8 * sin(current_time / t1);

var dx1 = xstart + 100 * sin(current_time / t2);
var dy1 = ystart + 100 * cos(current_time / t3);

var dx2 = xstart + 80 * cos(current_time / t2);
var dy2 = ystart + 80 * sin(current_time / t3);

draw_set_alpha(0.25);
draw_line_width_color(dx1, dy1, dx2, dy2, width, c_red, c_orange);

surface_reset_target();
draw_set_alpha(1);
draw_surface(canvas, 0, 0);

// Debugging
if(DEBUG) {
    draw_text(10, 10, "FPS: " + string(fps));
}