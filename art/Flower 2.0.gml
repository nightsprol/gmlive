#define main

// CONSTANTS1
DELTA_TIME  = 1200;  // ms
D_TIME2     = 777;

// CANVAS
WIDTH       = 1280;
HEIGHT      = 720;
BACK_COLOR  = c_white;

canvas      = surface_create(WIDTH, HEIGHT);

// DRAWING
MOVE_RADIUS = 170;
WAVE_SIZE   = 150;
LEAVES      = 100;

POINTS      = 10;

PEN_MAX     = 6;
PEN_MIN     = 1;

// STATES
SMOOTH      = true;
FADE_OUT    = false;
FADE_RATE   = 0.04;

// MOVEMENT
xstart      = WIDTH / 2;
ystart      = HEIGHT / 2;

px          = xstart;   // previous x
py          = ystart;   // previous y
dx          = xstart;   // current x
dy          = ystart;   // current y

#define step
var time = current_time / DELTA_TIME;
var time2 = current_time / D_TIME2;

// Oscillate around circle
var radius_wave = WAVE_SIZE * sin((time2) * LEAVES);

// UPDATE POSITION

// Get previous trajectory
var bx   = 2 * (dx + dx - px);
var by   = 2 * (dy + dy - py);

// Update trajectory
px      = dx;
py      = dy;
dx      = xstart + (MOVE_RADIUS + radius_wave) * cos(time);
dy      = ystart + (MOVE_RADIUS + radius_wave) * sin(time);

#define draw
var time = current_time / DELTA_TIME;
var time2 = current_time / D_TIME2;

// hue = [0, 255], saturation and value = [64, 192]
var hue         = 128 + 128 * sin(time2 * 0.1);
var saturation  = 128 + 64 * sin(time * 0.2);
var value       = 128 + 64 * sin(time * 0.3);
var color       = make_color_hsv(hue, saturation, value);

// Draw background
draw_set_color(BACK_COLOR);
draw_rectangle(0, 0, WIDTH, HEIGHT, false);

// Begin drawing to canvas
surface_set_target(canvas);

// Fade out whole surface slowly
if(FADE_OUT) {
    draw_set_alpha(FADE_RATE);
    draw_set_color(BACK_COLOR);
    
    draw_set_blend_mode(bm_zero);
    draw_rectangle(0, 0, WIDTH, HEIGHT, false);
    draw_set_blend_mode(bm_normal);
}

// Draw line to surface
draw_set_alpha(1);
draw_set_color(color);

if(smooth)
    draw_bezier_curve(px, py, dx, dy, bx, by, POINTS, PEN_MIN + PEN_MAX * abs(sin(time)));
else
    draw_line_width(px, py, dx, dy, PEN_MIN + PEN_MAX * abs(sin(time)));

// Reset surface target
surface_reset_target();

// Draw background color
draw_set_alpha(1);
draw_set_color(BACK_COLOR);
draw_rectangle(0, 0, WIDTH, HEIGHT, false);

// Draw canvas to window
draw_surface(canvas, 0, 0,);

// Draw pen over canvas
draw_set_color(color);
draw_set_alpha(1);
draw_circle(dx, dy, (PEN_MIN + PEN_MAX * abs(sin(time))) / 2, false);

draw_set_color(c_black);
draw_text(10, 10, "FPS: " + string(fps));

#define draw_bezier_curve
///draw_bezier_curve(startX, startY, endX, endY, bezierX, bezierY, [points], [width])

var startX  = argument[0];
var startY  = argument[1];
 
var endX    = argument[2];
var endY    = argument[3];
 
var bezierX = argument[4];
var bezierY = argument[5];

// Defaults
var points  = 100;
var width   = 1;

// Cascade arguments
switch(argument_count) {
    case 8: width   = argument[7];
    case 7: points  = argument[6];
}

var x1, y1;
var x2 = startX, y2 = startY;

for(var i = 1; i <= points; i++) {
    var f = i / points;     // fraction
    var fin = 1 - f;        // fraction inverse
    
    x1 = x2;                // xprevious
    y1 = y2;                // yprevious
    
    x2 = floor( (fin * fin * startX)
        + (2 * fin * f * bezierX) + (f * f * endX) );
    y2 = floor( (fin * fin * startY)
        + (2 * fin * f * bezierY) + (f * f * endY) );
    
    draw_line_width(x1, y1, x2, y2, width);
}