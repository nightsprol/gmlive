#define main
xstart = 512;
ystart = 512;
angle_speed = 5;

room_speed = 60;

canvas = surface_create(1280, 1280);

#define draw
var radius = 220;
x = xstart + (radius * cos(-current_time / 1000));
y = ystart + (radius * sin(-current_time / 1000));
image_angle += angle_speed;
angle_speed = 3 * sin(current_time / 1000);

var nodes = 1 + (current_time / 1000) % 8;
radius = 4 + (512 * abs(sin(current_time / 1000)));
node_radius = 16;

surface_set_target(canvas);

draw_primitive_begin(pr_trianglefan);
draw_vertex(x, y);

for(var i = 0; i <= nodes; i++) {
    _draw_vertex(i, nodes, radius);
}

_draw_vertex(0, nodes, radius);

draw_primitive_end();

var hue = 360 * sin(current_time / 1000);
draw_set_color(make_color_hsv(hue, 255, 255));

// Draw center of circle
draw_circle(x, y, node_radius, false);

surface_reset_target();
draw_surface(canvas, 0, 0);

#define _draw_vertex(i, nodes, radius)
var hue = (i + 1) * 360 / nodes;
draw_set_color(make_color_hsv(hue, 255, 255));

var rad = degtorad(((i - 1) * 360 / nodes) + image_angle);

var dx = x + radius * cos(rad);
var dy = y + radius * sin(rad);

draw_vertex(dx, dy);