# GMLive Scripts

This repository holds various scripts I've written in YellowAfterlife's GMLive. They range from scripted art to little games and experiments.

# Repository Structure

<pre>
gmlive/
├── .gitignore
├── art 					Scripts that generate art programmatically
├── functions				Generally math functions or reusable scripts
├── games					These are supposed to be played
├── page					WIP, I want to make dark mode GMLive
└── tests					Random test scripts, likely related to other scripts in the repo
</pre>